# Maintainer: Alexey Minnekhanov <alexeymin@postmarketos.org>
# Co-Maintainer: Luca Weiss <luca@z3ntu.xyz>
_flavor=postmarketos-qcom-msm8974
_config="config-$_flavor.$CARCH"

pkgname=linux-$_flavor
pkgver=5.16.0
pkgrel=0
_tag="v5.16.0-pmos"
pkgdesc="Kernel close to mainline with extra patches for Qualcomm MSM8974 devices"
arch="armv7"
_carch="arm"
url="https://kernel.org/"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-anbox
	pmb:kconfigcheck-containers
	pmb:kconfigcheck-iwd
	pmb:kconfigcheck-nftables
	pmb:kconfigcheck-zram
	"
makedepends="
	bison
	findutils
	flex
	gmp-dev
	mpc1-dev
	mpfr-dev
	openssl-dev
	perl
	postmarketos-installkernel
	"
source="
	https://gitlab.com/postmarketOS/linux-postmarketos/-/archive/$_tag/linux-postmarketos-$_tag.tar.bz2
	config-$_flavor.armv7
	"
builddir="$srcdir/linux-postmarketos-$_tag"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"
}

sha512sums="
69c1bdbebc248cb5a59622859d47388c17fd67ea435690d0d9315aaae8a855b5f5720013862be8ff32131b4971e507fe008eefe63f139cd6f813e1109ee3856b  linux-postmarketos-v5.16.0-pmos.tar.bz2
3467367032ca8a50688b34d57a50649526c1c6e6092bc1deb38c062aa3864f82de9332741b17db5a9a70654381636ebb90810880eb1fb5a29e513be639ebf2e0  config-postmarketos-qcom-msm8974.armv7
"
